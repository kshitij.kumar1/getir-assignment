const request = require("supertest");
const app = require("../../src/app");

const setupTestDB = require('../setupTestDB');

setupTestDB();

describe("Data API test", () => {
    test("This should return JSON object", async () => {
        const payload = {
            "startDate": "2015-01-26",
            "endDate": "2021-02-02",
            "minCount": 2700,
            "maxCount": 3000
        }

        const response = await request(app).post("/api/data").send(payload);
        expect(response.statusCode).toBe(200);
        
        expect(response.body).toEqual(expect.objectContaining({
            code: 0,
            msg: expect.any(String),
            records: expect.any(Array)
        }));
    });
});