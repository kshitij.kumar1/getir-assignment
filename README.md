## Getir Assignment
**Follow these steps to deploy the application using cli:**

 1. `git clone https://gitlab.com/kshitij.kumar1/getir-assignment.git`
 2. `cd getir-assignment`
 3. `npm install`
 4. `npm start` 

**To test the API using curl:**
`curl --location --request POST 'http://<domain>:3000/api/data'
--header 'Content-Type: application/json'
--data-raw '{
"startDate": "2016-01-26",
"endDate": "2018-02-02",
"minCount": 2700,
"maxCount": 3000
}'`

To check in local, replace `<domain>` with `localhost` and to check live instance, replace `<domain>` with `deploy.huviapp.co`

**To run test(s):** run `npm test` in root directory

**To check and test our API**:  navigate to http://deploy.huviapp.co:3000/api/docs/

**Note:** This application was built on modified boilerplate code from here: https://github.com/hagopj13/node-express-boilerplate