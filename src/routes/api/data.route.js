const express = require('express');
const { dataController } = require('../../controllers');
const { check, validationResult } = require('express-validator');

const router = express.Router();

router.post(
        '/',
        check('startDate').trim().isDate(),
        check('endDate').trim().isDate(),
        check('minCount').isInt(),
        check('maxCount').isInt(),
        function(req, res, next){
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() });
            }
            next()
        },
        dataController.getData
    );

module.exports = router;

/**
 * @swagger
 * tags:
 *   name: Data
 *   description: Data fetching for processing
 */

/**
 * @swagger
 * /data:
 *   post:
 *     summary: Gets data
 *     description: Gets data from mongodb cluster.
 *     tags: [Data]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             required:
 *               - startDate
 *               - endDate
 *               - minCount
 *               - maxCount
 *             properties:
 *               startDate:
 *                 type: string
 *                 format: date-time
 *               endDate:
 *                 type: string
 *                 format: date-time
 *               minCount:
 *                 type: number
 *               maxCount:
 *                  type: number
 *             example:
 *               startDate: 2015-01-26
 *               endDate: 2021-02-02
 *               minCount: 2700
 *               maxCount: 3000
 *     responses:
 *       "200":
 *         description: Created
 *         content:
 *           application/json:
 *             schema:
 *                $ref: '#/components/schemas/Data'
 */