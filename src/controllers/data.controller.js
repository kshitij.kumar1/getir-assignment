const httpStatus = require('http-status');
const dataModel = require('../models/data.model');

const ApiError = require('../libs/ApiError');
const catchAsync = require('../libs/catchAsync');

const getData = catchAsync(async (req, res) => {
  const { startDate, endDate, minCount, maxCount } = req.body;

  const data = await dataModel.aggregate([
    {
      '$project': {
        'totalCount': {
          '$sum': '$counts'
        }, 
        'key': true, 
        'createdAt': true, 
        '_id': false
      }
    }, {
      '$match': {
        '$and': [
          {
            'totalCount': {
              '$gte': minCount, 
              '$lte': maxCount
            }
          }, {
            'createdAt': {
              '$gte': new Date(startDate), 
              '$lte': new Date(endDate)
            }
          }
        ]
      }
    }
  ]);

  if (!data) {
    const return_data = {
      "code": -1,
      "msg": "Failed",
      "records": []
    }
  }

  const return_data = {
    "code": 0,
    "msg": "Success",
    "records": data
  }

  res.send(return_data);
});

module.exports = {
  getData
}