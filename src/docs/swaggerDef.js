const { version } = require('../../package.json');
const config = require('../config/config');

const swaggerDef = {
  openapi: '3.0.0',
  info: {
    title: 'Getir assignment API documentation',
    version,
    license: {
      name: 'ISC',
      url: 'https://gitlab.com/kshitij.kumar1/getir-assignment',
    },
  },
  servers: [
    {
      url: `http://localhost:${config.port}/api`,
    },
  ],
};

module.exports = swaggerDef;
