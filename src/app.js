const express = require('express');

const compression = require('compression');
const httpStatus = require('http-status');
const config = require('./config/config');

const routes = require('./routes/api');
const { errorConverter, errorHandler } = require('./middlewares/error');
const ApiError = require('./libs/ApiError');

const app = express();

// parse json request body
app.use(express.json());

// gzip compression
app.use(compression());

// api routes
app.use('/api', routes);

// send back a 404 error for any unknown api request
app.use((req, res, next) => {
  next(new ApiError(httpStatus.NOT_FOUND, 'Not found'));
});

// convert error to ApiError, if needed
app.use(errorConverter);

// handle error
app.use(errorHandler);

module.exports = app;
