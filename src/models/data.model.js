const mongoose = require('mongoose');

const dataSchema = new mongoose.Schema({
    key: {
        type: String
    },
    createdAt: {
        type: Date
    },
    totalCount: {
        type: Number
    }
});

const dataModel = mongoose.model('records', dataSchema);

module.exports = dataModel;